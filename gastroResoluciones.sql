
USE gastro;

/* 1 La primera, agarras los resultados y se saca el porcentaje
- Porcentaje de profesionales con y sin CV cargado: Del total de profesionales disponibles, cuantos
tienen cv cargado y cuantos no. */
SELECT count(*) from profesionales;
SELECT count(*) from profesionales WHERE cv_ingresado = 0;
SELECT count(*) from profesionales WHERE cv_ingresado = 1;



/*- 2 (a)Distribución geográfica de todos los profesionales por ciudad. (b)Distribución geográfica de todos los
profesionales que tienen cargado el CV.*/
SELECT ciudad, count(*) from profesionales group by(ciudad);  /* (a)cantidad de profesionales por ciudad */
SELECT coordenadas from profesionales WHERE coordenadas and cv_ingresado = 1; /* (b)coordenadas para cada profesional con cv */

/* 3 anuncios por ciudad y mes
- Distribución de los anuncios por ciudad y por mes, es decir cuantos anuncios son publicados por mes
en cada localidad. */
SELECT ciudad, MONTH(anuncios.fecha_alta) AS 'mes', count(*) AS 'cantidad' from anuncios
JOIN empresas group by MONTH(anuncios.fecha_alta), ciudad;


/* 4 Promedio de salario pretendido por tipo de puesto
- Salarios promedio pretendidos por tipo de puesto. */
SELECT anuncios.id_tipo_puesto, avg(cvs.salario_pretendido) from postulaciones 
JOIN profesionales ON postulaciones.id_profesional = profesionales.id
JOIN anuncios ON postulaciones.id_anuncio = anuncios.id
JOIN cvs ON profesionales.id = cvs.id_profesional
group by (anuncios.id_tipo_puesto);

/* 5 Promedio de salario pretendido por tipo de puesto y ciudad 
- Salarios promedio pretendidos por tipo de puesto y por ubicación geográfica. */
SELECT anuncios.id_tipo_puesto,  profesionales.ciudad, avg(cvs.salario_pretendido) from postulaciones 
JOIN profesionales ON postulaciones.id_profesional = profesionales.id
JOIN anuncios ON postulaciones.id_anuncio = anuncios.id
JOIN cvs ON profesionales.id = cvs.id_profesional
group by anuncios.id_tipo_puesto, profesionales.ciudad;

/* 6 coordenadas para cada profesionales
- Representación en un mapa de la ubicación de los profesionales.*/
SELECT coordenadas from profesionales;

/* 7 coordenadas para cada anuncio
- Representación en un mapa de la ubicación de los anuncios.*/
SELECT coordenadas from anuncios;

/* 8
Distribución de los cvs postulados por tipo de puesto.*/
SELECT anuncios.id_tipo_puesto, count(*) from postulaciones 
JOIN profesionales ON postulaciones.id_profesional = profesionales.id
JOIN anuncios ON postulaciones.id_anuncio = anuncios.id
JOIN cvs ON profesionales.id = cvs.id_profesional
group by anuncios.id_tipo_puesto;

/* 9 cvs postulados por tipo de puesto y ciudad
- Distribución de los cvs postulados por tipo de puesto. */

SELECT anuncios.id_tipo_puesto, ciudad, count(*) from cvs
JOIN profesionales ON cvs.id_profesional = profesionales.id
JOIN postulaciones ON profesionales.id = postulaciones.id_profesional
JOIN anuncios ON postulaciones.id_anuncio = anuncios.id
group by anuncios.id_tipo_puesto, profesionales.ciudad;

/* toma el top 10 de los anuncios con mas postulaciones
 intento del ejercicio con una banda de texto en la consigna */
SELECT anuncios.*, count(*)as total from anuncios
JOIN postulaciones ON anuncios.id = postulaciones.id_anuncio
group by anuncios.id
ORDER BY COUNT(*) DESC
LIMIT 10;


