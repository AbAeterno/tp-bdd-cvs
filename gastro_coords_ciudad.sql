USE gastro;

DROP FUNCTION IF EXISTS coords_ciudad;
DELIMITER $$
CREATE FUNCTION coords_ciudad(p_ciudad VARCHAR(100))
	RETURNS VARCHAR(50)
    NOT DETERMINISTIC
BEGIN
    DECLARE totalLat VARCHAR(30) DEFAULT 0;
    DECLARE totalLong VARCHAR(30) DEFAULT 0;
    DECLARE coordsCount INT DEFAULT 0;
    
    DECLARE finished BOOLEAN DEFAULT false;
	DECLARE v_coords VARCHAR(50) DEFAULT "";
	
	DECLARE coords_cursor CURSOR FOR
		SELECT coordenadas FROM profesionales WHERE ciudad = p_ciudad;
	
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET finished = true;
	
	OPEN coords_cursor;
		get_coords: LOOP
			FETCH coords_cursor INTO v_coords;
			
			IF finished = true THEN
				LEAVE get_coords;
			END IF;
            
            IF NOT v_coords = '' THEN
				SET coordsCount = coordsCount + 1;
				SET totalLat = totalLat + CAST(substring_index(v_coords, ', ', 1) AS DECIMAL(30, 15));
				SET totalLong = totalLong + CAST(substring_index(v_coords, ', ', -1) AS DECIMAL(30, 15));
			END IF;
		END LOOP get_coords;
	CLOSE coords_cursor;
    
    SET totalLat = totalLat / coordsCount;
    SET totalLong = totalLong / coordsCount;
    RETURN CONCAT_WS('/', totalLat, totalLong);
END $$
DELIMITER ;

# TEST
#SELECT coords_ciudad('LOMAS DE ZAMORA');