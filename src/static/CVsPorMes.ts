declare var cvsPostulados: any[];

// 2. Use the margin convention practice 
let marg: any = { top: 50, right: 50, bottom: 50, left: 50 }
let fwidth = window.innerWidth - marg.left - marg.right // Use the window's width 
let fheight = window.innerHeight - marg.top - marg.bottom; // Use the window's height

// The number of datapoints
var n = cvsPostulados.length;

// 5. X scale will use the index of our data
var xScale = d3.scaleLinear()
  .domain([0, n - 1]) // input
  .range([0, fwidth]); // output

// 6. Y scale will use the randomly generate number 
var yScale = d3.scaleLinear()
  .domain([0, Math.max.apply(null, cvsPostulados.map(function(cv) { return cv.cantidad }))]) // input 
  .range([fheight, 0]); // output 

// 7. d3's line generator
var line = d3.line()
  .x(function (d: any, i: number) { return xScale(i); }) // set the x values for the line generator
  .y(function (d: any) { return yScale(d.y); }) // set the y values for the line generator 
  .curve(d3.curveMonotoneX) // apply smoothing to the line

// 8. An array of objects of length N. Each object has key -> value pair, the key being "y" and the value is a random number
var dataset = cvsPostulados.map(function (d: any) { return { "y": d.cantidad, "mes": d.mes } })

// 1. Add the SVG to the page and employ #2
var svg = d3.select("body").append("svg")
  .attr("width", fwidth + marg.left + marg.right)
  .attr("height", fheight + marg.top + marg.bottom)
  .append("g")
  .attr("transform", "translate(" + marg.left + "," + marg.top + ")");

// 3. Call the x axis in a group tag
svg.append("g")
  .attr("class", "x axis")
  .attr("transform", "translate(0," + fheight + ")")
  .call(d3.axisBottom(xScale)); // Create an axis component with d3.axisBottom

// 4. Call the y axis in a group tag
svg.append("g")
  .attr("class", "y axis")
  .call(d3.axisLeft(yScale)); // Create an axis component with d3.axisLeft

// 9. Append the path, bind the data, and call the line generator 
svg.append("path")
  .datum(dataset) // 10. Binds data to the line 
  .attr("class", "line") // Assign a class for styling 
  .attr("d", line); // 11. Calls the line generator 

// Define the div for the tooltip
var div = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

const nombreMeses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
// 12. Appends a circle for each datapoint 
svg.selectAll(".dot")
  .data(dataset)
  .enter().append("circle") // Uses the enter().append() method
  .attr("class", "dot") // Assign a class for styling
  .attr("cx", function (d: any, i: number) { return xScale(i) })
  .attr("cy", function (d: any) { return yScale(d.y) })
  .attr("r", 8)
  .on("mouseover", function (d: any) {
      div.transition()
          .duration(200)
          .style("opacity", .9);
      div.html(nombreMeses[d.mes] + ": " + d.y + " postulaciones")
          .style("left", (d3.event.pageX) + "px")
          .style("top", (d3.event.pageY - 28) + "px");
  })
  .on("mouseout", function () {
      div.transition()
          .duration(500)
          .style("opacity", 0);
  });