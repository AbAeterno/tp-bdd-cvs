declare var d3: any;
declare var puestos: any[];

const margen2 = { top: 20, right: 20, bottom: 70, left: 50 };
const width4 = 900 - margen2.left - margen2.right;
const height4 = 400 - margen2.top - margen2.bottom;

var x = d3.scale.ordinal().rangeRoundBands([0, width4], .05);

var y = d3.scale.linear().range([height4, 0]);

var xAxis = d3.svg.axis()
  .scale(x)
  .orient("bottom");

var yAxis = d3.svg.axis()
  .scale(y)
  .orient("left")
  .ticks(10);

var svg = d3.select("body").append("svg")
  .attr("width", width4 + margen2.left + margen2.right)
  .attr("height", height4 + margen2.top + margen2.bottom)
  .append("g")
  .attr("transform",
    "translate(" + margen2.left + "," + margen2.top + ")");

x.domain(puestos.map(function(obj) { return obj.id; }));
y.domain([0, Math.max.apply(null, puestos.map(function(obj) { return obj.pretendido; })) + 1000]);

svg.append("g")
  .attr("class", "x axis")
  .attr("transform", "translate(0," + height4 + ")")
  .call(xAxis)
  .selectAll("text")
  .style("text-anchor", "end")
  .attr("dx", "-.8em")
  .attr("dy", "-.55em")
  .attr("transform", "rotate(-90)");

svg.append("g")
  .attr("class", "y axis")
  .call(yAxis)
  .append("text")
  .attr("transform", "rotate(-90)")
  .attr("y", 6)
  .attr("dy", ".71em")
  .style("text-anchor", "end")
  .text("Pretendido");

// Define the div for the tooltip
var div = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

svg.selectAll("bar")
  .data(puestos)
  .enter()
  .append("rect")
  .style("fill", "steelblue")
  .attr("x", function (d: any) { return x(d.id); })
  .attr("width", x.rangeBand())
  .attr("y", function (d: any) { return y(d.pretendido); })
  .attr("height", function (d: any) { return height4 - y(d.pretendido); })
  .on("mouseover", function (d: any) {
      div.transition()
          .duration(200)
          .style("opacity", .9);
      div.html("Pretendido: $" + d.pretendido.toFixed(2))
          .style("left", (d3.event.pageX) + "px")
          .style("top", (d3.event.pageY - 28) + "px");
  })
  .on("mouseout", function () {
      div.transition()
          .duration(500)
          .style("opacity", 0);
  });