declare var objs: any[];

// set the dimensions of the graph
const width = 800;
const height = 400;

// append the svg object to the body of the page
var svg = d3.select("#my_dataviz")
    .append("svg")
    .attr("width", width + 100)
    .attr("height", height + 50)
    .append("g")
    .attr("transform",
        "translate(" + 60 + "," + 10 + ")");

// Add X axis
var x = d3.scaleLinear()
    .domain([0, 12])
    .range([0, width]);
svg.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x));

// Add Y axis
var y = d3.scaleLinear()
    .domain([0, 1000])
    .range([height, 0]);
svg.append("g")
    .call(d3.axisLeft(y));

// Define the div for the tooltip
var div = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

// Add dots
var positions: any = {};
for (let obj of objs) {
    svg.append("circle")
        .attr("cx", obj.mes * (width / 12))
        .attr("cy", height - obj.anuncios)
        .attr("r", 4)
        .attr("opacity", 0.6)
        .style("fill", "#69b3a2")
        .on("mouseover", function () {
            div.transition()
                .duration(200)
                .style("opacity", .9);
            div.html(obj.ciudad + "<br>Anuncios: " + obj.anuncios)
                .style("left", (d3.event.pageX) + "px")
                .style("top", (d3.event.pageY - 28) + "px");
        })
        .on("mouseout", function () {
            div.transition()
                .duration(500)
                .style("opacity", 0);
        });
}