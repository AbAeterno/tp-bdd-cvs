declare var d3: any;
declare var postulaciones: any[];

const margenf = { top: 20, right: 20, bottom: 70, left: 50 }
const width2 = 900 - margenf.left - margenf.right
const height2 = 400 - margenf.top - margenf.bottom;

var x = d3.scale.ordinal().rangeRoundBands([0, width2], .05);

var y = d3.scale.linear().range([height2, 0]);

var xAxis = d3.svg.axis()
  .scale(x)
  .orient("bottom");

var yAxis = d3.svg.axis()
  .scale(y)
  .orient("left")
  .ticks(10);

var svg = d3.select("body").append("svg")
  .attr("width", width2 + margenf.left + margenf.right)
  .attr("height", height2 + margenf.top + margenf.bottom)
  .append("g")
  .attr("transform",
    "translate(" + margenf.left + "," + margenf.top + ")");

x.domain(postulaciones.map(function(obj) { return obj.id; }));
y.domain([0, Math.max.apply(null, postulaciones.map(function(obj) { return obj.cantidadCVs; })) + 100]);

svg.append("g")
  .attr("class", "x axis")
  .attr("transform", "translate(0," + height2 + ")")
  .call(xAxis)
  .selectAll("text")
  .style("text-anchor", "end")
  .attr("dx", "-.8em")
  .attr("dy", "-.55em")
  .attr("transform", "rotate(-90)");

svg.append("g")
  .attr("class", "y axis")
  .call(yAxis)
  .append("text")
  .attr("transform", "rotate(-90)")
  .attr("y", 6)
  .attr("dy", ".71em")
  .style("text-anchor", "end")
  .text("Postulaciones");

// Define the div for the tooltip
var div = d3.select("body").append("div")
    .attr("class", "tooltip")
    .style("opacity", 0);

svg.selectAll("bar")
  .data(postulaciones)
  .enter()
  .append("rect")
  .style("fill", "steelblue")
  .attr("x", function (d: any) { return x(d.id); })
  .attr("width", x.rangeBand())
  .attr("y", function (d: any) { return y(d.cantidadCVs); })
  .attr("height", function (d: any) { return height2 - y(d.cantidadCVs); })
  .on("mouseover", function (d: any) {
      div.transition()
          .duration(200)
          .style("opacity", .9);
      div.html("Postulaciones: " + d.cantidadCVs)
          .style("left", (d3.event.pageX) + "px")
          .style("top", (d3.event.pageY - 28) + "px");
  })
  .on("mouseout", function () {
      div.transition()
          .duration(500)
          .style("opacity", 0);
  });;