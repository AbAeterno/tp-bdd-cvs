var mysql = require('mysql');

const dbconfig = {
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'gastro'
};

export class DBHandler {
    private dbConn: any;

    constructor() {
        this.dbConn = mysql.createConnection(dbconfig);
        this.dbConn.connect();
    }

    /** callback(total, conCV, sinCV) */
    public profesionalesCVs(callback: Function) {
        var total = 0;
        var conCV = 0;
        var sinCV = 0;
        this.dbConn.query('SELECT count(*) AS total FROM profesionales', function(error: any, results: any, fields: any) {
            if (error) throw error;
            total = results[0].total;

            this.dbConn.query('SELECT count(*) AS sinCV FROM profesionales WHERE cv_ingresado = 0;', function(error: any, results: any, fields: any) {
                if (error) throw error;
                sinCV = results[0].sinCV;
                conCV = total - sinCV;

                callback(total, conCV, sinCV);
            });
        }.bind(this));
    }

    /** callback(ciudades) */
    public profsPorCiudad(callback: Function, conCV: boolean) {
        var query = 'SELECT coordenadas, ciudad from profesionales;';
        if (conCV) {
            query = 'SELECT coordenadas, ciudad from profesionales where cv_ingresado = 1;';
        }
        var ciudades: any = {};
        this.dbConn.query(query, function(error: any, results: any, fields: any) {
            if (error) throw error;
            
            for (let prof of results) {
                if (!prof.ciudad) {
                    continue;
                }

                if (!ciudades.hasOwnProperty(prof.ciudad)) {
                    // Formato diccionario de ciudades
                    ciudades[prof.ciudad] = {
                        coords: new Array(),
                        centroLat: 0,
                        centroLong: 0,
                        cantProfs: 0
                    };
                }

                ciudades[prof.ciudad].coords.push(prof.coordenadas);
            }
            
            for (let key in ciudades) {
                var ciudad = ciudades[key];

                var cantProfs = ciudad.coords.length;
                var tmpCentroLat = 0;
                var tmpCentroLong = 0;
                for (let coords of ciudad.coords) {
                    var coordsSplit = coords.replace(/\s+/g, '').split(',');
                    if (+coordsSplit[0] != 0 && +coordsSplit[1] != 0) {
                        tmpCentroLat = tmpCentroLat + +coordsSplit[0];
                        tmpCentroLong = tmpCentroLong + +coordsSplit[1];
                    } else {
                        cantProfs = cantProfs - 1
                    }
                }

                ciudad.centroLat = tmpCentroLat / cantProfs;
                ciudad.centroLong = tmpCentroLong / cantProfs;
                ciudad.cantProfs = cantProfs;
            }

            callback(ciudades);
        });
    }

    /** callback(objs)      
     * objs -> array de objetos con el siguiente formato: {     
     *      ciudad: string,     
     *      mes: number,        
     *      anuncios: number        
     * }
     */
    public anunciosCiudadMes(callback: Function) {
        var objs: any[] = new Array();
        var query: string = "SELECT ciudad, MONTH(anuncios.fecha_alta) AS 'mes', count(*) AS 'cantidad' from anuncios JOIN empresas group by MONTH(anuncios.fecha_alta), ciudad;"

        this.dbConn.query(query, function(error: any, results: any, fields: any) {
            if (error) throw error;
            
            for (let res of results) {
                objs.push({
                    ciudad: res.ciudad,
                    mes: res.mes,
                    anuncios: res.cantidad
                });
            }

            callback(objs);
        });
    }

    /** callback(puestos)       
     * puestos -> array de objetos con el siguiente formato: {      
     *      id: number,     
     *      pretendido: number      
     * }
     */
    public salarioPretendidoPorPuesto(callback: Function) {
        var puestos: any[] = new Array();
        var query: string = "SELECT anuncios.id_tipo_puesto, avg(cvs.salario_pretendido) AS 'pretendido' from postulaciones";
        query = query + " JOIN profesionales ON postulaciones.id_profesional = profesionales.id";
        query = query + " JOIN anuncios ON postulaciones.id_anuncio = anuncios.id";
        query = query + " JOIN cvs ON profesionales.id = cvs.id_profesional";
        query = query + " group by (anuncios.id_tipo_puesto)";

        this.dbConn.query(query, function(error: any, results: any, fields: any) {
            if (error) throw error;

            for (let res of results) {
                puestos.push({
                    id: res.id_tipo_puesto,
                    pretendido: res.pretendido
                });
            }

            callback(puestos);
        });
    }

    /** callback(ciudades)       
     * ciudades -> diccionario de objetos con el siguiente formato:     
     * ciudades[nombreCiudad] = {       
     *      lat: number,     
     *      long: number,      
     *      puestos: diccionario de objetos -> {        
     *          6: <promedio de salario pretendido para puesto 6>,      
     *          8: <promedio de salario pretendido para puesto 8>,      
     *          25: <promedio de salario pretendido para puesto 25>,        
     *          ...     
     *      }       
     * }
     */
    public salarioPretPuestoCiudad(callback: Function) {
        var ciudades: any = {};
        var query: string = "SELECT anuncios.id_tipo_puesto, profesionales.ciudad, avg(cvs.salario_pretendido) AS 'pretendido' from postulaciones"
        query = query + " JOIN profesionales ON postulaciones.id_profesional = profesionales.id";
        query = query + " JOIN anuncios ON postulaciones.id_anuncio = anuncios.id";
        query = query + " JOIN cvs ON profesionales.id = cvs.id_profesional";
        query = query + " group by anuncios.id_tipo_puesto, profesionales.ciudad;";

        this.dbConn.query(query, function(error: any, results: any, fields: any) {
            if (error) throw error;

            for (let res of results) {
                if (!ciudades.hasOwnProperty(res.ciudad)) {
                    ciudades[res.ciudad] = {
                        lat: 0,
                        long: 0,
                        puestos: {}
                    }
                }
                ciudades[res.ciudad].puestos[res.id_tipo_puesto] = res.pretendido;
            }

            // Coordenadas para las ciudades
            this.dbConn.query("SELECT ciudad, coords_ciudad(ciudad) AS 'coords' FROM profesionales GROUP BY ciudad;", function(error: any, results: any, fields: any) {
                if (error) throw error;

                for (let res of results) {
                    if (ciudades.hasOwnProperty(res.ciudad)) {
                        var coords = res.coords.split('/');
                        ciudades[res.ciudad].lat = +coords[0];
                        ciudades[res.ciudad].long = +coords[1];
                    }
                }

                callback(ciudades);
            });
        }.bind(this));
    }

    /** callback(profs)        
     * profs -> array de objetos con el siguiente formato: {       
     *      nombreCompleto: string,     
     *      lat: number,        
     *      long: number        
     * }
     */
    public coordsProfesionales(callback: Function) {
        var profs: any[] = new Array();

        this.dbConn.query('SELECT nombre, apellido, coordenadas from profesionales;', function(error: any, results: any, fields: any) {
            if (error) throw error;

            for (let res of results) {
                var resCoords = res.coordenadas
                var coordsSplit = resCoords.replace(/\s+/g, '').split(',');
                if (+coordsSplit[0] != 0 && +coordsSplit[1] != 0) {
                    profs.push({
                        nombreCompleto: res.apellido + ' ' + res.nombre,
                        lat: +coordsSplit[0],
                        long: +coordsSplit[1]
                    });
                }
            }
            callback(profs);
        });
    }

    /** callback(anuncios)        
     * anuncios -> array de objetos con el siguiente formato: {       
     *      titulo: string,     
     *      lat: number,        
     *      long: number        
     * }
     */
    public coordsAnuncios(callback: Function) {
        var anuncios: any[] = new Array();

        this.dbConn.query('SELECT coordenadas, titulo from anuncios;', function(error: any, results: any, fields: any) {
            if (error) throw error;

            for (let res of results) {
                var resCoords = res.coordenadas
                var coordsSplit = resCoords.replace(/\s+/g, '').split(',');
                if (+coordsSplit[0] != 0 && +coordsSplit[1] != 0) {
                    anuncios.push({
                        titulo: res.titulo,
                        lat: +coordsSplit[0],
                        long: +coordsSplit[1]
                    });
                }
            }
            callback(anuncios);
        });
    }

    /** callback(postulaciones)        
     * postulaciones -> array de objetos con el siguiente formato: {       
     *      id: number,     
     *      cantidadCVs: number        
     * }
     */
    public distribPostulacionesCVs(callback: Function) {
        var postulaciones: any[] = new Array();
        var query: string = "SELECT anuncios.id_tipo_puesto, count(*) AS 'cantidad' from postulaciones ";
        query = query + " JOIN profesionales ON postulaciones.id_profesional = profesionales.id";
        query = query + " JOIN anuncios ON postulaciones.id_anuncio = anuncios.id";
        query = query + " JOIN cvs ON profesionales.id = cvs.id_profesional";
        query = query + " group by (anuncios.id_tipo_puesto)";

        this.dbConn.query(query, function(error: any, results: any, fields: any) {
            if (error) throw error;

            for (let res of results) {
                postulaciones.push({
                    id: res.id_tipo_puesto,
                    cantidadCVs: res.cantidad
                });
            }

            callback(postulaciones);
        });
    }

    /** callback(CVs)        
     * CVs -> array de objetos con el siguiente formato: {       
     *      mes: number,     
     *      cantidad: number        
     * }
     */
    public CVsPorMes(callback: Function) {
        var CVs: any[] = new Array();
        var query = "SELECT MONTH(fecha_hora) AS 'mes', count(*) AS 'cantidad' FROM postulaciones GROUP BY MONTH(fecha_hora);"

        this.dbConn.query(query, function(error: any, results: any, fields: any) {
            if (error) throw error;

            for (let res of results) {
                CVs.push({
                    mes: res.mes,
                    cantidad: res.cantidad
                });
            }

            callback(CVs);
        });
    }
}