import express from "express";
import path from "path";
import { DBHandler } from "./db-handler";

const app = express();
const port = 8080;
var testDB = new DBHandler();

app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.use("/static", express.static(path.join(__dirname, "static")));

app.get("/", (req, res) => {
    testDB.anunciosCiudadMes(() => {});
    res.render("index");
});

app.get("/profesionalesCVs", (req, res) => {
    testDB.profesionalesCVs((profTotal: number, profConCV: number, profSinCV: number) => {
        res.render("profesionalesCVs", {profTotal, profConCV, profSinCV});
    });
});

app.get("/distribProfsPorCiudad", (req, res) => {
    testDB.profsPorCiudad((ciudades: any) => {
        testDB.profsPorCiudad((ciudadesConCV: any) => {
            res.render("distribProfsPorCiudad", {ciudades, ciudadesConCV});
        }, true);
    }, false);
});

app.get("/anunciosCiudadMes", (req, res) => {
    testDB.anunciosCiudadMes((objs: any) => {
        res.render("anunciosCiudadMes", {objs});
    });
});

app.get("/salarioPretendidoPorPuesto", (req, res) => {
    testDB.salarioPretendidoPorPuesto((puestos: any) => {
        res.render("salarioPretendidoPorPuesto", {puestos});
    });
});

app.get("/salarioPretPuestoCiudad", (req, res) => {
    testDB.salarioPretPuestoCiudad((ciudades: any) => {
        res.render("salarioPretPuestoCiudad", {ciudades});
    });
});

app.get("/coordsProfesionales", (req, res) => {
    testDB.coordsProfesionales((profs: any) => {
        res.render("coordsProfesionales", {profs});
    });
});

app.get("/coordsAnuncios", (req, res) => {
    testDB.coordsAnuncios((anuncios: any) => {
        res.render("coordsAnuncios", {anuncios});
    });
});

app.get("/distribPostulacionesCVs", (req, res) => {
    testDB.distribPostulacionesCVs((postulaciones: any) => {
        res.render("distribPostulacionesCVs", {postulaciones});
    });
});

app.get("/CVsPorMes", (req, res) => {
    testDB.CVsPorMes((CVs: any) => {
        res.render("CVsPorMes", {CVs});
    });
});

app.listen(port, () => {
    console.log(`Servidor iniciado: http://localhost:${port}`);
});